﻿using AutoMapper;
using DK.ApplicationCore.DTOs;
using DK.ApplicationCore.Interfaces.Services;
using DK.ApplicationCore.Services;
using DK.Infrastructure.Data;
using DK.Infrastructure.Mappings;
using DK.Infrastructure.Repository;
using DK.UI.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace DK.Integration.Test
{
    public class TestEmployeController
    {
        private EmployeContext employeContext;

        public TestEmployeController()
        {
            employeContext = DK.Test.Configuration.Config.GetContext();
        }
        private EmployeSalaryController GetController()
        {
            EmployeMasterSalaryRepository employeMasterSalaryRepository = new EmployeMasterSalaryRepository(DK.Test.Configuration.Config.GetContext());
            ConfigurationRepository configurationRepository = new ConfigurationRepository(DK.Test.Configuration.Config.GetContext());
            IEmployeMasterSalaryService employeMasterSalaryService = new EmployeMasterSalaryService(configurationRepository, employeMasterSalaryRepository);
            ICalculateEmployesSalaryService calculateEmployesSalaryService = new CalculateEmployesSalaryService(configurationRepository);

            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutomapperProfile());
            });
            var mapper = mockMapper.CreateMapper();
            var controller = new EmployeSalaryController(employeMasterSalaryService, mapper, calculateEmployesSalaryService);


            return controller;
        }

        [Fact]
        public async Task Search_ReturnsAViewResult_WithEmployeGeneralDataDto()
        {
            // Arrange

            await using var context = DK.Test.Configuration.Config.GetContext();
            // Act
            var controller = GetController();
            var result = await controller.Search("2000");

            // Assert
            var viewResult = Assert.IsType<PartialViewResult>(result);
            var model = Assert.IsAssignableFrom<EmployeGeneralDataDto>(
                viewResult.ViewData.Model);
            Assert.NotNull(model);
        }
        [Fact]
        public async Task Search_ReturnsAViewResult_With_No_Found_Employe()
        {
            // Arrange

            using (var context = DK.Test.Configuration.Config.GetContext())
            {
                EmployeMasterSalaryRepository employeMasterSalaryRepository = new EmployeMasterSalaryRepository(context);
                ConfigurationRepository configurationRepository = new ConfigurationRepository(context);
                IEmployeMasterSalaryService employeMasterSalaryService = new EmployeMasterSalaryService(configurationRepository, employeMasterSalaryRepository);
                ICalculateEmployesSalaryService calculateEmployesSalaryService = new CalculateEmployesSalaryService(configurationRepository);

                var mockMapper = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile(new AutomapperProfile());
                });
                var mapper = mockMapper.CreateMapper();

                var controller = new EmployeSalaryController(employeMasterSalaryService, mapper, calculateEmployesSalaryService);

                // Act
                var result = await controller.Search("35000");

                // Assert
                var viewResult = Assert.IsType<PartialViewResult>(result);
                var model = Assert.IsAssignableFrom<EmployeGeneralDataDto>(
                    viewResult.ViewData.Model);
                Assert.Equal("N/A", model.IdentificationNumber);
            }
        }


        [Fact]
        public async Task Create_ReturnView_No_Errors()
        {
            // Arrange

            var controller = GetController();

            // Act
            var newEmploye = new EmployeGeneralDataDto
            {
                EmployeCode = "2001",
                EmployeName = "Roberto",
                EmployeSurname = "Martinez",
                BeginDate = DateTime.Now,
                Birthday = DateTime.Now.AddYears(-35),
                Division = "Divicion 1",
                Grade = 18,
                IdentificationNumber = "051019952326",
                Office = "Oficce 2",
                Position = "Position 1",
                LinesData = new List<EmployeMasterSalaryDto>
                {
                    new EmployeMasterSalaryDto
                    {
                        BaseSalary = 3455,
                        Commission = 500,
                        CompensationBonus = 350,
                        Contributions = -150,
                        Month = 12,
                        Year = 2020,
                        ProductionBonus = 350,
                        OtherIncome = 1525,
                        TotalSalary = 7425
                    },
                    new EmployeMasterSalaryDto
                    {
                        BaseSalary = 3150,
                        Commission = 500,
                        CompensationBonus = 350,
                        Contributions = -150,
                        Month = 12,
                        Year = 2020,
                        ProductionBonus = 350,
                        OtherIncome = 1525,
                        TotalSalary = 7425
                    }
                }

            };
            var result = await controller.Create(newEmploye);

            // Assert
            Assert.IsType<JsonResult>(result);
            var removedObject = employeContext.EmployeMasterSalaries.Where(l => l.EmployeCode == "2001" && l.Year == 2020 && l.Month == 12);
            if (removedObject.Any())
            {
                employeContext.EmployeMasterSalaries.RemoveRange(removedObject);
                employeContext.SaveChanges();
            }

            //ReturnObject deserializedResult = JsonConvert.DeserializeObject<ReturnObject>(jsonResult.Value.ToString());
            //Assert.True(deserializedResult.Passed);
        }
        [Fact]
        public async Task Create_ReturnView_Duplicate_Month_Same_Year()
        {
            // Arrange



                var controller = GetController();

                // Act
                var newEmploye = new EmployeGeneralDataDto
                {
                    EmployeCode = "2000",
                    EmployeName = "Juan",
                    EmployeSurname = "Perez",
                    BeginDate = DateTime.Now,
                    Birthday = DateTime.Now.AddYears(-15),
                    Division = "Divicion 1",
                    Grade = 15,
                    IdentificationNumber = "051019952326",
                    Office = "Oficce 1",
                    Position = "Position 1",
                    LinesData = new List<EmployeMasterSalaryDto>
                    {
                        new EmployeMasterSalaryDto
                        {
                            BaseSalary = 3455,
                            Commission = 500,
                            CompensationBonus = 350,
                            Contributions = -150,
                            Month = 12,
                            Year = 2020,
                            ProductionBonus = 350,
                            OtherIncome = 1525,
                            TotalSalary = 7425
                        },
                        new EmployeMasterSalaryDto
                        {
                            BaseSalary = 3150,
                            Commission = 500,
                            CompensationBonus = 350,
                            Contributions = -150,
                            Month = 11,
                            Year = 2020,
                            ProductionBonus = 350,
                            OtherIncome = 1525,
                            TotalSalary = 7425
                        }
                    }

                };
                var result = await controller.Create(newEmploye);

                // Assert
                var jsonResult = Assert.IsType<JsonResult>(result);
                employeContext.EmployeMasterSalaries.RemoveRange(employeContext.EmployeMasterSalaries.Where(l => l.EmployeCode == "2001" && l.Year == 2020 && l.Month == 12));
                employeContext.SaveChanges();
                var serialiceJson = JsonConvert.SerializeObject(jsonResult.Value);
                ReturnObject deserializedResult = JsonConvert.DeserializeObject<ReturnObject>(serialiceJson);
                Assert.False(deserializedResult.Passed);
        }


        [Fact]
        public void Create_Data()
        {
            // Arrange

            var controller = GetController();

            // Act

            var result = controller.Create();

            // Assert
            Assert.IsType<ViewResult>(result);


        }

    }
}
