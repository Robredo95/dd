﻿namespace DK.Integration.Test
{
    internal class ReturnObject
    {
        public bool Passed { get; set; }

        public string PageResult { get; set; }
    }
}