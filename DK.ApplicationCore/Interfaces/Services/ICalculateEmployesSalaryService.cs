﻿using System.Threading.Tasks;

namespace DK.ApplicationCore.Interfaces.Services
{
    public interface ICalculateEmployesSalaryService
    {
        Task<decimal> CalculateOtherIncome(decimal baseSalary, decimal commission);
        public Task<decimal> CalculateTotalSalary(decimal baseSalary, decimal productionBonus, decimal compensationBonus, decimal otherIncome, decimal contributions);
        Task<decimal> CalculateBonusLastSalaries(decimal baseSalary);
    }
}
