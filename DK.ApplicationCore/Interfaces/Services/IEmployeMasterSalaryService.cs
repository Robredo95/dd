﻿using DK.ApplicationCore.Entity;
using DK.ApplicationCore.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DK.ApplicationCore.Interfaces.Services
{
    public interface IEmployeMasterSalaryService
    {

        Task<Result> Create(EmployeMasterSalary employeMasterSalary);

        Task<Result> Create(List<EmployeMasterSalary> employeMasterSalary);

        Task<List<EmployeMasterSalary>> GetEmployeMasterSalaries();

        Task<List<EmployeMasterSalary>> Filter(Expression<Func<EmployeMasterSalary, bool>> predicate);
    }
}
