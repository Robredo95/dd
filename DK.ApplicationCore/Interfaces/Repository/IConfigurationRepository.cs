﻿using DK.ApplicationCore.Entity;

namespace DK.ApplicationCore.Interfaces.Repository
{
    public interface IConfigurationRepository : IRepository<Configuration>
    {
    }
}
