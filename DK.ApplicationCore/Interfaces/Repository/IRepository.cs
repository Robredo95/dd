﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace DK.ApplicationCore.Interfaces.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Add(TEntity entity);

        List<TEntity> AddRange(List<TEntity> entity);
        List<TEntity> GetAll();

        TEntity GetById(int entity);

        void Delete(TEntity entity);

        List<TEntity> Search(Expression<Func<TEntity, bool>> predicate);
    }
}
