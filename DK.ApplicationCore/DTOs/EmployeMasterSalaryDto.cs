﻿using System;
using System.ComponentModel;

namespace DK.ApplicationCore.DTOs
{
    public class EmployeMasterSalaryDto
    {
        public int Id { get; set; }

        [DisplayName("Año")]
        public int Year { get; set; }


        [DisplayName("Mes")]
        public int Month { get; set; }

        [DisplayName("Oficina")]
        public string Office { get; set; }


        [DisplayName("Codigo Empleado")]
        public string EmployeCode { get; set; }

        [DisplayName("Nombre")]

        public string EmployeName { get; set; }

        [DisplayName("Apellido")]
        public string EmployeSurname { get; set; }


        [DisplayName("Division")]

        public string Division { get; set; }


        [DisplayName("Posicion")]
        public string Position { get; set; }


        [DisplayName("Grado")]
        public int Grade { get; set; }


        [DisplayName("Fecha Contratacion")]

        public DateTime BeginDate { get; set; }


        [DisplayName("Fecha de Nacimiento")]

        public DateTime Birthday { get; set; }

        [DisplayName("Identificacion")]

        public string IdentificationNumber { get; set; }

        [DisplayName("Salario Base")]
        public decimal BaseSalary { get; set; }

        [DisplayName("Bono produccion")]
        public decimal ProductionBonus { get; set; }

        [DisplayName("Bono compensacion")]
        public decimal CompensationBonus { get; set; }

        [DisplayName("Comision")]
        public decimal Commission { get; set; }

        [DisplayName("Contribucuion")]
        public decimal Contributions { get; set; }

        [DisplayName("Otros Ingresos")]
        public decimal? OtherIncome { get; set; }

        [DisplayName("Salario Total")]
        public decimal? TotalSalary { get; set; }
    }
}
