﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DK.ApplicationCore.DTOs.Atributes
{
    public class DateBithDayValidateAttribute : RangeAttribute
    {
        public DateBithDayValidateAttribute()
          : base(typeof(DateTime), DateTime.Now.AddYears(-150).ToShortDateString(), DateTime.Now.ToShortDateString()) { }
    }
}
