﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace DK.ApplicationCore.DTOs
{
    public class EmployeGeneralDataDto
    {
        [DisplayName("Codigo Empleado")]
        public string EmployeCode { get; set; }

        [DisplayName("Nombre")]

        public string EmployeName { get; set; }

        [DisplayName("Apellido")]
        public string EmployeSurname { get; set; }


        [DisplayName("Division")]

        public string Division { get; set; }


        [DisplayName("Posicion")]
        public string Position { get; set; }


        [DisplayName("Grado")]
        public int Grade { get; set; }


        [DisplayName("Fecha Contratacion")]

        public DateTime BeginDate { get; set; }


        [DisplayName("Fecha de Nacimiento")]

        public DateTime Birthday { get; set; }

        [DisplayName("Identificacion")]

        public string IdentificationNumber { get; set; }

        [DisplayName("Oficina")]

        public string Office { get; set; }

        public List<EmployeMasterSalaryDto> LinesData { get; set; }

    }
}
