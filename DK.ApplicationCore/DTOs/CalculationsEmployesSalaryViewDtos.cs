﻿using System;

namespace DK.ApplicationCore.DTOs
{
    public class CalculationsEmployesSalaryViewDtos
    {
        public string EmployeCode { get; set; }
        public string Office { get; set; }
        public int Grade { get; set; }
        public string EmployeName { get; set; }
        public string EmployeSurname { get; set; }
        public string EmployeFullName { get { return string.Concat(EmployeName, " ", EmployeSurname); } }
        public string Divicion { get; set; }
        public string Position { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime BirthDay { get; set; }
        public string IdentificationNumber { get; set; }

        public decimal TotalSalary { get; set; }
    }
}
