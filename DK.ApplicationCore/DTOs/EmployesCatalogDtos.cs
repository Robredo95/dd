﻿using System;

namespace DK.ApplicationCore.DTOs
{
    public class EmployesCatalogDtos
    {
        public string EmployeCode { get; set; }
        public string EmployeName { get; set; }
        public string EmployeSurname { get; set; }
        public string EmployeFullName { get { return string.Concat(EmployeName, " ", EmployeSurname); } }
        public string Divicion { get; set; }
        public string Position { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime BirthDay { get; set; }
        public string IdentificationNumber { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commission { get; set; }
        public decimal Contributions { get; set; }
        public decimal? OtherIncome { get; set; }
        public decimal? TotalSalary { get; set; }
    }
}
