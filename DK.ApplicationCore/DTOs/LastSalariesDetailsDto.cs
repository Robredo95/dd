﻿namespace DK.ApplicationCore.DTOs
{
    public class LastSalariesDetailsDto
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public decimal BaseSalary { get; set; }

        public decimal Bonus { get; set; }
    }
}
