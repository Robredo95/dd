﻿namespace DK.ApplicationCore.Services
{
    public class Result
    {
        public bool Passed { get; set; }
        public string MessageError { get; set; }

    }
}
