﻿using DK.ApplicationCore.Interfaces.Repository;
using DK.ApplicationCore.Interfaces.Services;
using System;
using System.Threading.Tasks;

namespace DK.ApplicationCore.Services
{
    public class CalculateEmployesSalaryService : ICalculateEmployesSalaryService
    {
        private readonly IConfigurationRepository _configurationRepository;
        public CalculateEmployesSalaryService(IConfigurationRepository configurationRepository)
        {
            this._configurationRepository = configurationRepository;
        }
        public async Task<decimal> CalculateOtherIncome(decimal baseSalary, decimal commission)
        {
            var configPercentOtherIncome = _configurationRepository.GetById(1);
            if (configPercentOtherIncome == null)
            {
                throw new Exception("No hay definido porcentaje para calculo de otros ingresos");
            }

            return ((baseSalary + commission) * Convert.ToDecimal(configPercentOtherIncome.Value) / 100) + commission;
        }

        public async Task<decimal> CalculateBonusLastSalaries(decimal baseSalary)
        {
            var configBonusLastSalaries = _configurationRepository.GetById(3);
            if (configBonusLastSalaries == null)
            {
                throw new Exception("No hay definido porcentaje para calculo de otros ingresos");
            }

            return (baseSalary / Convert.ToDecimal(configBonusLastSalaries.Value));
        }

        public async Task<decimal> CalculateTotalSalary(decimal baseSalary, decimal productionBonus, decimal compensationBonus, decimal otherIncome, decimal contributions)
        {
            var configPercentOtherIncome = _configurationRepository.GetById(2);
            if (configPercentOtherIncome == null)
            {
                throw new Exception("No hay definido porcentaje para calculo de total Salario");
            }
            return baseSalary + productionBonus + (compensationBonus * Convert.ToDecimal(configPercentOtherIncome.Value) / 100) + otherIncome - contributions;
        }
    }
}
