﻿using DK.ApplicationCore.Entity;
using DK.ApplicationCore.Interfaces.Repository;
using DK.ApplicationCore.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DK.ApplicationCore.Services
{
    public class EmployeMasterSalaryService : IEmployeMasterSalaryService
    {
        private readonly IConfigurationRepository _configurationRepository;
        private readonly IEmployeMasterSalaryRepository _employeMasterSalaryRepository;
        public EmployeMasterSalaryService(IConfigurationRepository configurationRepository, IEmployeMasterSalaryRepository employeMasterSalaryRepository)
        {
            this._configurationRepository = configurationRepository;
            this._employeMasterSalaryRepository = employeMasterSalaryRepository;
        }
        public async Task<Result> Create(EmployeMasterSalary employeMasterSalary)
        {
            var employeMasterSalaryReturn = _employeMasterSalaryRepository.Add(employeMasterSalary);

            if (employeMasterSalaryReturn == null)
            {
                return new Result { Passed = false, MessageError = "Ha ocurrido un error al ingresar el registro" };
            }
            return new Result { Passed = true };
        }
        public async Task<Result> Create(List<EmployeMasterSalary> employeMasterSalary)
        {
            var employeMasterSalaryReturn = _employeMasterSalaryRepository.AddRange(employeMasterSalary);

            if (employeMasterSalaryReturn == null)
            {
                return new Result { Passed = false, MessageError = "Ha ocurrido un error al ingresar la lista de registros" };
            }
            return new Result { Passed = true };
        }
        public async Task<List<EmployeMasterSalary>> Filter(Expression<Func<EmployeMasterSalary, bool>> predicate)
        {
            return _employeMasterSalaryRepository.Search(predicate);
        }

        public async Task<List<EmployeMasterSalary>> GetEmployeMasterSalaries()
        {
            return _employeMasterSalaryRepository.GetAll();
        }

    }
}
