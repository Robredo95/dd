﻿using System;
using System.Threading.Tasks;
using DK.ApplicationCore.Interfaces.Repository;
using DK.ApplicationCore.Services;
using DK.Infrastructure.Repository;
using DK.Test.Configuration;
using Xunit;

namespace DK.Unit.Test.AplicationCore.Services
{
    public class CalculateEmployesSalaryServiceTests
    {
        [Fact]
        public async Task CalculateOtherIncomeTest()
        {
            IConfigurationRepository configurationRepository = new ConfigurationRepository(Config.GetContext());
            CalculateEmployesSalaryService calculateEmployesSalaryService = new CalculateEmployesSalaryService(configurationRepository);

            //configurarion otherIncome 0.08 Represent 8%
            //Formula = otherIncome -= ((baseSalary + commission) * configPercentOtherIncome.Value) + commission;
            //Asert ((3500 + 500)*0.08)+500 = 820;

            var result = await calculateEmployesSalaryService.CalculateOtherIncome(3500, 500);

            Assert.Equal(820,result);
        }
        [Fact]
        public async Task CalculateOtherIncomeNoConfigDataExceptionTest()
        {
            IConfigurationRepository configurationRepository = new ConfigurationRepository(Config.GetContext());
            CalculateEmployesSalaryService calculateEmployesSalaryService = new CalculateEmployesSalaryService(configurationRepository);

            //configurarion otherIncome 0.08 Represent 8%
            //Formula = otherIncome -= ((baseSalary + commission) * configPercentOtherIncome.Value) + commission;
            //Asert ((3500 + 500)*0.08)+500 = 820;

            var configPercentOtherIncome = configurationRepository.GetById(1);
            configurationRepository.Delete(configPercentOtherIncome!);
           await Assert.ThrowsAsync<Exception>(() => calculateEmployesSalaryService.CalculateOtherIncome(3500, 500));

        }
        [Fact]
        public async Task CalculateBonusLastSalariesTest()
        {
            IConfigurationRepository configurationRepository = new ConfigurationRepository(Config.GetContext());
            CalculateEmployesSalaryService calculateEmployesSalaryService = new CalculateEmployesSalaryService(configurationRepository);

            //configurarion configBonusLastSalaries 3
            //Formula = CalculateBonusLastSalaries/3;
            //Asert 3000/3 = 1000;

           var result = await calculateEmployesSalaryService.CalculateBonusLastSalaries(3000);

           Assert.Equal(1000,result);

        }
        [Fact]
        public async Task CalculateBonusLastSalariesTestNoConfigDataExceptionTest()
        {
            IConfigurationRepository configurationRepository = new ConfigurationRepository(Config.GetContext());
            CalculateEmployesSalaryService calculateEmployesSalaryService = new CalculateEmployesSalaryService(configurationRepository);

            //configurarion otherIncome 0.08 Represent 8%
            //Formula = otherIncome -= ((baseSalary + commission) * configPercentOtherIncome.Value) + commission;
            //Asert ((3500 + 500)*0.08)+500 = 820;

            var configPercentOtherIncome = configurationRepository.GetById(3);
            configurationRepository.Delete(configPercentOtherIncome!);

            await Assert.ThrowsAsync<Exception>(() => calculateEmployesSalaryService.CalculateBonusLastSalaries(3000));

        }

        
        [Fact]
        public async Task CalculateTotalSalaryTest()
        {
            IConfigurationRepository configurationRepository = new ConfigurationRepository(Config.GetContext());
            CalculateEmployesSalaryService calculateEmployesSalaryService = new CalculateEmployesSalaryService(configurationRepository);

            //configurarion otherIncome 0.08 Represent 8%
            //Formula = TotalSalary = BaseSalary + ProductionBonus + (CompensationBonus * 75%) + OtherIncome - Contributions;
            //Asert ((3500 + 500 + 350*0.75)+155-695 = 3722.5;



            var result = await calculateEmployesSalaryService.CalculateTotalSalary(3500, 500, 350, 155, 695);

            Assert.Equal(3722.5M, result);
        }

        [Fact]
        public async Task CalculateTotalSalaryNoConfigDataExceptionTest()
        {
            IConfigurationRepository configurationRepository = new ConfigurationRepository(Config.GetContext());
            CalculateEmployesSalaryService calculateEmployesSalaryService = new CalculateEmployesSalaryService(configurationRepository);

            //configurarion otherIncome 0.08 Represent 8%
            //Formula = otherIncome -= ((baseSalary + commission) * configPercentOtherIncome.Value) + commission;
            //Asert ((3500 + 500)*0.08)+500 = 820;

            var configPercentOtherIncome = configurationRepository.GetById(2);
            configurationRepository.Delete(configPercentOtherIncome!);

            await Assert.ThrowsAsync<Exception>(() => calculateEmployesSalaryService.CalculateTotalSalary(3500, 500, 350, 155, 695));

        }
    }
}