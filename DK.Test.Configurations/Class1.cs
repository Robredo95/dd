﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DK.Test.Configurations
{
    public static class Configs
    {
        private static EmployeContext GetContext()
        {
            var options = new DbContextOptionsBuilder<EmployeContext>()
                            .UseInMemoryDatabase(databaseName: "EmployeDB")
                            .Options;

            var context = new EmployeContext(options);

            if (!context.EmployeMasterSalaries.Any())
            {


                context.EmployeMasterSalaries.Add(new EmployeMasterSalary
                {
                    Id = 1,
                    EmployeCode = "2000",
                    EmployeName = "Juan",
                    EmployeSurname = "Perez",
                    BeginDate = DateTime.Now,
                    Birthday = DateTime.Now.AddYears(-15),
                    Division = "Divicion 1",
                    Grade = 15,
                    IdentificationNumber = "051019952326",
                    Office = "Oficce 1",
                    Position = "Position 1",
                    BaseSalary = 3500,
                    Commission = 500,
                    CompensationBonus = 350,
                    Contributions = -150,
                    Month = 12,
                    Year = 2020,
                    ProductionBonus = 350,
                    OtherIncome = 1525,
                    TotalSalary = 7503

                });
                context.SaveChanges();
            }
            if (!context.Configurations.Any())
            {
                var configPercentOtherIncome = new Configuration { Key = 1, Value = "0.08" };
                context.Configurations.Add(configPercentOtherIncome);

                var totalSalaryCompensationPorcentRule = new Configuration { Key = 2, Value = "0.75" };
                context.Configurations.Add(totalSalaryCompensationPorcentRule);

                var bonusLastSalary = new Configuration { Key = 3, Value = "3" };
                context.Configurations.Add(bonusLastSalary);
                context.SaveChanges();
            }
            return context;

        }
    }
}
