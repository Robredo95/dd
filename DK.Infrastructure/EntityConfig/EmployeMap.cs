﻿using DK.ApplicationCore.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DK.Infrastructure.EntityConfig
{
    public class EmployeMap : IEntityTypeConfiguration<EmployeMasterSalary>
    {
        public void Configure(EntityTypeBuilder<EmployeMasterSalary> builder)
        {
            builder.Property(l => l.EmployeCode)
                .HasColumnType("varchar(10)")
                .IsRequired();

            builder.Property(l => l.EmployeName)
                .HasColumnType("varchar(150)")
                .IsRequired();

            builder.Property(l => l.EmployeSurname)
                .HasColumnType("varchar(150)")
                .IsRequired();

            builder.Property(l => l.IdentificationNumber)
               .HasColumnType("varchar(10)")
               .IsRequired();

            builder.Property(l => l.IdentificationNumber)
              .HasColumnType("varchar(10)")
              .IsRequired();

            builder.Property(l => l.OtherIncome)
              .HasColumnType("decimal(18,4)")
              .IsRequired();
            builder.Property(l => l.Commission)
              .HasColumnType("decimal(18,4)")
              .IsRequired();
            builder.Property(l => l.CompensationBonus)
             .HasColumnType("decimal(18,4)")
             .IsRequired();
            builder.Property(l => l.Contributions)
             .HasColumnType("decimal(18,4)")
             .IsRequired();
            builder.Property(l => l.TotalSalary)
             .HasColumnType("decimal(18,4)")
             .IsRequired();

            builder.Property(l => l.BaseSalary)
            .HasColumnType("decimal(18,4)")
            .IsRequired();

            builder.Property(l => l.ProductionBonus)
          .HasColumnType("decimal(18,4)")
          .IsRequired();
        }
    }
}
