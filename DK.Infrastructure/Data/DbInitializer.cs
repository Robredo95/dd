﻿using Bogus;
using DK.ApplicationCore.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DK.Infrastructure.Data
{
    public static class DbInitializer
    {
        public static void Initialice(this ModelBuilder modelBuilder)
        {
            var configPercentOtherIncome = new Configuration { Key = 1, Value = "8" };
            modelBuilder.Entity<Configuration>().HasData(configPercentOtherIncome);

            var totalSalaryCompensationPorcentRule = new Configuration { Key = 2, Value = "75" };
            modelBuilder.Entity<Configuration>().HasData(totalSalaryCompensationPorcentRule);

            var bonusLastSalary = new Configuration { Key = 3, Value = "3" };
            modelBuilder.Entity<Configuration>().HasData(bonusLastSalary);

            var offices = new List<Office>();

            offices.Add(new Office { Name = "Office F" });
            offices.Add(new Office { Name = "Office X" });
            offices.Add(new Office { Name = "Office Z" });
            offices.Add(new Office { Name = "Office R" });

            var divicions = new List<Divicion>();

            divicions.Add(new Divicion { Name = "OPERATION" });
            divicions.Add(new Divicion { Name = "SALES" });
            divicions.Add(new Divicion { Name = "MARKETING" });
            divicions.Add(new Divicion { Name = "CUSTOMER CARE" });

            var positions = new List<Position>();

            positions.Add(new Position { Name = "CARGO MANAGER" });
            positions.Add(new Position { Name = "HEAD OF CARGO" });
            positions.Add(new Position { Name = "DIRECTOR" });
            positions.Add(new Position { Name = "ASSISTANT" });

            var employes = new Faker<Employes>()
                .RuleFor(o => o.EmployeCode, f => Convert.ToString(200000 + f.IndexFaker))
                .RuleFor(o => o.EmployeName, f => f.Person.FirstName)
                .RuleFor(o => o.EmployeSurname, f => f.Person.LastName)
                .RuleFor(o => o.Office, f => f.PickRandom(offices).Name)
                .RuleFor(o => o.Grade, f => f.Random.Int(5, 25))
                .RuleFor(o => o.Position, f => f.PickRandom(positions).Name)
                .RuleFor(o => o.IdentificationNumber, f => (2000000 + f.IndexFaker).ToString())
                .RuleFor(o => o.Division, f => f.PickRandom(divicions).Name)
                .RuleFor(o => o.Birthaday, f => RandomBithDay())
                .RuleFor(o => o.BeginDate, f => RandomBeginDate())
                .Generate(100);

            var employesMasterSalaryDetails = new List<EmployeMasterSalaryDetails>();
            var ramdomValues = new Random();
            for (int i = 0; i < 6; i++)
            {
                var dateCalculate = DateTime.Now.AddMonths(-(6 - i));

                employesMasterSalaryDetails.AddRange(employes.Select(l => new EmployeMasterSalaryDetails
                {
                    EmployeCode = l.EmployeCode,
                    Year = dateCalculate.Year,
                    Month = dateCalculate.Month,
                    BaseSalary = Convert.ToDecimal(ramdomValues.Next(3000, 5000) + (ramdomValues.Next(0, 100) / 1)),
                    Commission = Convert.ToDecimal(ramdomValues.Next(500, 1500) + (ramdomValues.Next(0, 100) / 1)),
                    CompensationBonus = Convert.ToDecimal(ramdomValues.Next(350, 1000) + (ramdomValues.Next(0, 100) / 1)),
                    ProductionBonus = Convert.ToDecimal(ramdomValues.Next(450, 1200) + (ramdomValues.Next(0, 100) / 1)),
                    Contributions = Convert.ToDecimal(ramdomValues.Next(350, 1000) + (ramdomValues.Next(0, 100) / 1)) * -1
                })
                    );


            }
            var employesMasterSalary = employes.Join(employesMasterSalaryDetails, a => a.EmployeCode, b => b.EmployeCode, (a, b) => new { a, b })
                .Select((l, index) => new EmployeMasterSalary
                {
                    Id = index + 1,
                    EmployeCode = l.a.EmployeCode,
                    EmployeName = l.a.EmployeName,
                    EmployeSurname = l.a.EmployeSurname,
                    BeginDate = l.a.BeginDate,
                    Birthday = l.a.BeginDate,
                    Division = l.a.Division,
                    Grade = l.a.Grade,
                    IdentificationNumber = l.a.IdentificationNumber,
                    Office = l.a.Office,
                    Position = l.a.Position,
                    BaseSalary = l.b.BaseSalary,
                    Commission = l.b.Commission,
                    CompensationBonus = l.b.CompensationBonus,
                    Contributions = l.b.Contributions,
                    Month = l.b.Month,
                    Year = l.b.Year,
                    ProductionBonus = l.b.ProductionBonus,
                    OtherIncome = ((l.b.BaseSalary + l.b.Commission) * Convert.ToDecimal(configPercentOtherIncome.Value) / 100) + l.b.Commission,
                    TotalSalary = l.b.BaseSalary + l.b.ProductionBonus + (l.b.CompensationBonus * Convert.ToDecimal(configPercentOtherIncome.Value) / 100) + (((l.b.BaseSalary + l.b.Commission) * Convert.ToDecimal(configPercentOtherIncome.Value) / 100) + l.b.Commission) - l.b.Contributions

                });
            modelBuilder.Entity<EmployeMasterSalary>().HasData(employesMasterSalary.OrderBy(l => l.Id));

        }
        static DateTime RandomBithDay()
        {
            DateTime start = new DateTime(1980, 1, 1);
            var ramdom = new Random();
            int range = (DateTime.Today - start).Days;
            return start.AddDays(ramdom.Next(range));
        }
        static DateTime RandomBeginDate()
        {
            DateTime start = new DateTime(2005, 1, 1);
            var ramdom = new Random();
            int range = (DateTime.Today - start).Days;
            return start.AddDays(ramdom.Next(range));
        }
    }

    internal class EmployeMasterSalaryDetails
    {
        public string EmployeCode { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public decimal BaseSalary { get; set; }
        public decimal ProductionBonus { get; set; }
        public decimal CompensationBonus { get; set; }
        public decimal Commission { get; set; }
        public decimal Contributions { get; set; }

    }

    internal class Employes
    {
        public string Office { get; set; }
        public string EmployeCode { get; set; }
        public string EmployeName { get; set; }
        public string EmployeSurname { get; set; }

        public string Division { get; set; }
        public string Position { get; set; }
        public int Grade { get; set; }
        public DateTime BeginDate { get; set; }
        public DateTime Birthaday { get; set; }
        public string IdentificationNumber { get; set; }
    }

    internal class Position
    {
        public string Name { get; set; }
    }

    internal class Divicion
    {
        public string Name { get; set; }
    }

    internal class Office
    {
        public string Name { get; set; }
    }
}
