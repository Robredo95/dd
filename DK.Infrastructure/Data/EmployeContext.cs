﻿using DK.ApplicationCore.Entity;
using DK.Infrastructure.EntityConfig;
using Microsoft.EntityFrameworkCore;

namespace DK.Infrastructure.Data
{
    public class EmployeContext : DbContext
    {
        public EmployeContext(DbContextOptions<EmployeContext> options) : base(options)
        {

        }
        public DbSet<EmployeMasterSalary> EmployeMasterSalaries { get; set; }
        public DbSet<Configuration> Configurations { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration<EmployeMasterSalary>(new EmployeMap());
            modelBuilder.Initialice();
        }
    }
}
