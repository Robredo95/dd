﻿using DK.ApplicationCore.Interfaces.Repository;
using DK.Infrastructure.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DK.Infrastructure.Repository
{
    public class EfRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly EmployeContext EmployeContext;
        public EfRepository(EmployeContext employeContext)
        {
            this.EmployeContext = employeContext;
        }
        public TEntity Add(TEntity entity)
        {
            EmployeContext.Set<TEntity>().Add(entity);
            EmployeContext.SaveChanges();
            return entity;
        }

        public List<TEntity> AddRange(List<TEntity> entity)
        {
            EmployeContext.Set<TEntity>().AddRange(entity);
            EmployeContext.SaveChanges();
            return entity;
        }

        public List<TEntity> GetAll()
        {
            return EmployeContext.Set<TEntity>().ToList();
        }

        public TEntity GetById(int id)
        {
            return EmployeContext.Set<TEntity>().Find(id);
        }

        public void Delete(TEntity entity)
        {
            this.EmployeContext.Set<TEntity>().Remove(entity);
            this.EmployeContext.SaveChanges();
        }

        public List<TEntity> Search(Expression<Func<TEntity, bool>> predicate)
        {
            return EmployeContext.Set<TEntity>().Where(predicate).ToList();
        }
    }
}
