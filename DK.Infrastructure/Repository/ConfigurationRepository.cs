﻿using DK.ApplicationCore.Entity;
using DK.ApplicationCore.Interfaces.Repository;
using DK.Infrastructure.Data;

namespace DK.Infrastructure.Repository
{
    public class ConfigurationRepository : EfRepository<Configuration>, IConfigurationRepository
    {
        public ConfigurationRepository(EmployeContext employeContext) : base(employeContext)
        {
        }
    }
}
