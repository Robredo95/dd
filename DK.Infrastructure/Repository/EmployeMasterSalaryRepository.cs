﻿using DK.ApplicationCore.Entity;
using DK.ApplicationCore.Interfaces.Repository;
using DK.Infrastructure.Data;

namespace DK.Infrastructure.Repository
{
    public class EmployeMasterSalaryRepository : EfRepository<EmployeMasterSalary>, IEmployeMasterSalaryRepository
    {
        public EmployeMasterSalaryRepository(EmployeContext employeContext) : base(employeContext)
        {
        }
    }
}
