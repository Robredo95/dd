﻿using AutoMapper;
using DK.ApplicationCore.DTOs;
using DK.ApplicationCore.Entity;

namespace DK.Infrastructure.Mappings
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<EmployeMasterSalary, EmployeMasterSalaryDto>();
            CreateMap<EmployeMasterSalaryDto, EmployeMasterSalary>();

            CreateMap<EmployeGeneralDataDto, EmployeMasterSalary>();
            CreateMap<EmployeMasterSalary, EmployeGeneralDataDto>();

            CreateMap<CalculationsEmployesSalaryViewDtos, EmployeMasterSalary>();
            CreateMap<EmployeMasterSalary, CalculationsEmployesSalaryViewDtos>();

            CreateMap<LastSalariesDetailsDto, EmployeMasterSalary>();
            CreateMap<EmployeMasterSalary, LastSalariesDetailsDto>();

            CreateMap<LastSalariesDto, EmployeMasterSalary>();
            CreateMap<EmployeMasterSalary, LastSalariesDto>();
        }
    }
}
