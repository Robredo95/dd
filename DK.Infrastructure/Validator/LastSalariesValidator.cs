﻿using DK.ApplicationCore.DTOs;
using FluentValidation;
using System.Globalization;

namespace DK.Infrastructure.Validator
{
    public class LastSalariesValidator : AbstractValidator<LastSalariesDto>
    {
        public LastSalariesValidator()
        {
            ValidatorOptions.Global.LanguageManager.Culture = new CultureInfo("es-Mx");

            RuleFor(l => l.EmployeCode)
                .NotEmpty()
                .WithName("Codigo Empleado");
        }
    }
}
