﻿using DK.ApplicationCore.DTOs;
using FluentValidation;
using System;
using System.Globalization;

namespace DK.Infrastructure.Validator
{
    public class EmployeMasterSalaryValidator : AbstractValidator<EmployeMasterSalaryDto>
    {
        public EmployeMasterSalaryValidator()
        {
            ValidatorOptions.Global.LanguageManager.Culture = new CultureInfo("es-Mx");

            RuleFor(l => l.EmployeCode).MaximumLength(10)
                .NotEmpty()
                .WithName("Codigo Empleado");


            RuleFor(l => l.EmployeName).Length(150)
                .NotEmpty()
                .WithName("Nombre")
                .Matches(@"^[a-zA-Z-']*$")
                .WithMessage("El campo {PropertyName} solo soporta letras.");

            RuleFor(l => l.EmployeSurname).Length(150)
                .NotEmpty()
                .WithName("Apellido")
                .Matches(@"^[a-zA-Z-']*$")
                .WithMessage("El campo {PropertyName} solo soporta letras.");

            RuleFor(l => l.IdentificationNumber).Length(10)
                .NotEmpty()
                .WithName("Identificacion")
                .Matches(@"^[a-zA-Z-']*$")
                .WithMessage("El campo {PropertyName} solo soporta letras.");

            RuleFor(l => l.Grade)
                .NotEmpty()
                .ExclusiveBetween(1, 12)
                .WithName("Grado")
                .WithMessage("El campo {PropertyName} debe ser un valor entre {From} a {To}");

            RuleFor(l => l.Month)
                .NotEmpty()
                .ExclusiveBetween(1, 12)
                .WithName("Mes")
                .WithMessage("El campo {PropertyName} debe ser un valor entre {From} a {To}");

            RuleFor(l => l.Year)
                .NotEmpty()
                .ExclusiveBetween(1900, 2999)
                .WithName("Año")
                .WithMessage("El campo {PropertyName} debe ser un valor entre {From} a {To}");

            RuleFor(l => l.BeginDate)
                  .NotNull()
                .WithMessage("La {PropertyName} no puede ser Nullo.")
               .GreaterThan(DateTime.Now.AddYears(-100))
               .WithName("Fecha Contratacion");

            RuleFor(l => l.Birthday)
                 .NotNull()
               .WithMessage("La {PropertyName} no puede ser Nullo.")
              .GreaterThan(DateTime.Now.AddYears(-100))
              .WithName("Fecha Nacimiento");

            RuleFor(l => l.BaseSalary)
                  .NotNull()
                .WithMessage("El {PropertyName} no puede ser Nullo.")
               .GreaterThanOrEqualTo(0M)
               .WithMessage("El {PropertyName} no puede ser Negativo.")
               .WithName("Salario Base");

            RuleFor(l => l.CompensationBonus)
                  .NotNull()
                .WithMessage("El {PropertyName} no puede ser Nullo.")
               .GreaterThanOrEqualTo(0M)
               .WithMessage("El {PropertyName} no puede ser Negativo.")
               .WithName("Bono de Compensacion");

            RuleFor(l => l.ProductionBonus)
                  .NotNull()
                .WithMessage("El {PropertyName} no puede ser Nullo.")
               .GreaterThanOrEqualTo(0M)
               .WithMessage("El {PropertyName} no puede ser Negativo.")
               .WithName("Bono de Produccion");

            RuleFor(l => l.Commission)
                  .NotNull()
                .WithMessage("El {PropertyName} no puede ser Nullo.")
               .GreaterThanOrEqualTo(0M)
               .WithMessage("El {PropertyName} no puede ser Negativo.")
               .WithName("Comision");

            RuleFor(l => l.Contributions)
                  .NotNull()
                .WithMessage("El {PropertyName} no puede ser Nullo.")
               .LessThanOrEqualTo(0M)
               .WithMessage("El {PropertyName} debe ser Negativo.")
               .WithName("Contribucion");

        }

    }
}
