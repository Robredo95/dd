﻿using AutoMapper;
using DK.ApplicationCore.DTOs;
using DK.ApplicationCore.Entity;
using DK.ApplicationCore.Interfaces.Services;
using DK.UI.Web.Controllers.Untils;
using DK.UI.Web.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DK.UI.Web.Controllers
{
    public class EmployeSalaryController : Controller
    {

        private readonly IEmployeMasterSalaryService _employeMasterSalaryService;
        private readonly ICalculateEmployesSalaryService _calculateEmployesSalaryService;
        private readonly IMapper _mapper;
        private EmployeMasterSalary _employeMasterSalary;

        public EmployeSalaryController(IEmployeMasterSalaryService employeMasterSalaryService, IMapper mapper, ICalculateEmployesSalaryService calculateEmployesSalaryService)
        {
            this._employeMasterSalaryService = employeMasterSalaryService;
            this._calculateEmployesSalaryService = calculateEmployesSalaryService;
            this._mapper = mapper;
        }

        public async Task<ActionResult> Search(string code)
        {
            if (string.IsNullOrEmpty(code)) throw new ArgumentException("Value cannot be null or empty.", nameof(code));
            var dataEmploye = (await _employeMasterSalaryService.GetEmployeMasterSalaries()).Find(l => l.EmployeCode == code);
            if (dataEmploye == null)
            {
                return PartialView("Details", new EmployeGeneralDataDto
                {
                    Birthday = new DateTime(1900, 01, 01),
                    EmployeName = "N/A",
                    EmployeCode = "N/A",
                    EmployeSurname = "N/A",
                    BeginDate = new DateTime(1900, 01, 01),
                    Division = "N/A",
                    Grade = 0,
                    IdentificationNumber = "N/A",
                    Position = "N/A"

                });
            }
            var dataEmployeRetun = _mapper.Map<EmployeGeneralDataDto>(dataEmploye);
            return PartialView("Details", dataEmployeRetun);
        }

        // GET: EmployeSalaryController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: EmployeSalaryController/Create
        [HttpPost]
        public async Task<ActionResult> Create(EmployeGeneralDataDto employeGeneralDataDto)
        {
            try
            {
                if (employeGeneralDataDto.EmployeCode == null)
                {
                    return Json(new { Passed = false, PageResult = "Codigo de empleado Null o vacio." });
                }
                if (!(await _employeMasterSalaryService.GetEmployeMasterSalaries()).Where(l => l.EmployeCode == employeGeneralDataDto.EmployeCode).Any())
                {
                    return Json(new { Passed = false, PageResult = "Codigo del empleado no existe." });
                }
                if (employeGeneralDataDto.LinesData == null)
                {
                    return Json(new { Passed = false, PageResult = "Registre al menos un detalle economico." });
                }

                var employeMasterSalaryData = employeGeneralDataDto.LinesData.Select(async l => new EmployeMasterSalaryDto
                {
                    EmployeCode = employeGeneralDataDto.EmployeCode,
                    EmployeName = employeGeneralDataDto.EmployeName,
                    EmployeSurname = employeGeneralDataDto.EmployeSurname,
                    Birthday = employeGeneralDataDto.Birthday,
                    BeginDate = employeGeneralDataDto.BeginDate,
                    IdentificationNumber = employeGeneralDataDto.IdentificationNumber,
                    Division = employeGeneralDataDto.Division,
                    Office = employeGeneralDataDto.Office,
                    Position = employeGeneralDataDto.Position,
                    Grade = employeGeneralDataDto.Grade,
                    BaseSalary = l.BaseSalary,
                    Commission = l.Commission,
                    CompensationBonus = l.CompensationBonus,
                    Contributions = l.Contributions,
                    Month = l.Month,
                    Year = l.Year,
                    ProductionBonus = l.ProductionBonus,
                    OtherIncome = await _calculateEmployesSalaryService.CalculateOtherIncome(l.BaseSalary, l.Commission),
                    TotalSalary = await _calculateEmployesSalaryService.CalculateTotalSalary(l.BaseSalary, l.ProductionBonus, l.CompensationBonus, (await _calculateEmployesSalaryService.CalculateOtherIncome(l.BaseSalary, l.Commission)), l.Contributions)

                }).Select(l => l.Result).ToList();

                var valideYearAndMonthData = (await _employeMasterSalaryService.GetEmployeMasterSalaries())

                    .Join(employeMasterSalaryData, a => a.EmployeCode, b => b.EmployeCode, (a, b) => new { DbData = a, newData = b })
                    .Where(l => l.DbData.Year == l.newData.Year && l.DbData.Month == l.newData.Month).ToList();
                if (valideYearAndMonthData.Any())
                {
                    var firtDuplicateDate = valideYearAndMonthData.FirstOrDefault();
                    if (firtDuplicateDate != null)
                        return Json(new
                        {
                            Passed = false,
                            PageResult =
                                $"El mes {firtDuplicateDate.newData.Month} para el año {firtDuplicateDate.newData.Year} del empleado {firtDuplicateDate.DbData.EmployeCode} ya esta registrado."
                        });
                }
                var dataToCreate = _mapper.Map<List<EmployeMasterSalary>>(employeMasterSalaryData);
                var result = await _employeMasterSalaryService.Create(dataToCreate);

                if (result.Passed)
                {
                    return Json(new { Passed = true, PageResult = "" });
                }
                else
                {
                    return Json(new { Passed = false, PageResult = result.MessageError });
                }

            }
            catch (Exception)
            {
                return Json(new { Passed = false, PageResult = $"Ocurrio un error inesperado comuniquese con el administrador del sistema." });
            }
        }

        // GET: EmployeSalaryController/Edit/5
        public ActionResult Successful()
        {
            return View();
        }

        public ActionResult ErrorView(string message)
        {
            var actionResult = View(new ErrorViewModel {MessageError = message});
            return actionResult;
        }

        public ActionResult CalculationMasterViews()
        {
            return View();
        }

        public async Task<ActionResult> CalculationMasterDetailsViews(string option, string employeCode)
        {
            if (option == null)
            {
                return PartialView(new List<CalculationsEmployesSalaryViewDtos>());
            }


            var employesCatalog = (await _employeMasterSalaryService.GetEmployeMasterSalaries())
                .GroupBy(l => new { l.EmployeCode, l.EmployeName, l.EmployeSurname, l.Division, l.Grade, l.Position, l.BeginDate, l.Birthday, l.IdentificationNumber, l.Office })
                .Select(l => new { l.Key, Year = l.Max(v => v.Year), Month = l.Where(v => v.Year == l.Max(employeMasterSalary => employeMasterSalary.Year)).Max(employeMasterSalary => employeMasterSalary.Month), DataGrouping = l.Where(f => f.Year == l.Max(employeMasterSalary => employeMasterSalary.Year)) })
                .Select((l, i) =>
                {
                    {
                        bool Func(EmployeMasterSalary x)
                        {
                            return x.Month == l.Month;
                        }


                        _employeMasterSalary = l.DataGrouping.FirstOrDefault(Func);
                        if (_employeMasterSalary != null)
                            return new
                            {
                                Id = i,
                                l.Key.EmployeCode,
                                l.Key.EmployeName,
                                l.Key.EmployeSurname,
                                l.Key.BeginDate,
                                l.Key.Grade,
                                BirthDay = l.Key.BeginDate,
                                Divicion = l.Key.Division,
                                l.Key.IdentificationNumber,
                                l.Key.Position,
                                BaseSalary = l.DataGrouping.Any()
                                    ? _employeMasterSalary.BaseSalary
                                    : 0,
                                Commission = l.DataGrouping.Any()
                                    ? _employeMasterSalary.Commission
                                    : 0,
                                CompensationBonus = l.DataGrouping.Any()
                                    ? _employeMasterSalary.CompensationBonus
                                    : 0,
                                ProductionBonus = l.DataGrouping.Any()
                                    ? _employeMasterSalary.ProductionBonus
                                    : 0,
                                Contributions = l.DataGrouping.Any()
                                    ? _employeMasterSalary.Contributions
                                    : 0,
                                l.Key.Office
                            };
                    }

                    return null;
                });
            var catalog = employesCatalog.ToList();
            var modelEmploye = catalog.FirstOrDefault(l => l.EmployeCode == employeCode);

            Func<CalculationsEmployesSalaryViewDtos, bool> filterData;
            switch ((FilterGrouping)Convert.ToInt32(option))
            {
                case FilterGrouping.OfficeAndGrade:
                    filterData = e => modelEmploye == null || (e.Office == modelEmploye.Office && e.Grade == modelEmploye.Grade && e.EmployeCode != modelEmploye.EmployeCode);
                    break;
                case FilterGrouping.AllOfficeAndGrade:
                    filterData = e => modelEmploye == null || (e.Grade == modelEmploye.Grade && e.EmployeCode != modelEmploye.EmployeCode);
                    break;
                case FilterGrouping.PositionAndGrade:
                    filterData = e => modelEmploye == null || (e.Position == modelEmploye.Position && e.Grade == modelEmploye.Grade && e.EmployeCode != modelEmploye.EmployeCode);
                    break;
                case FilterGrouping.AllpositionAndGrade:
                    filterData = e => modelEmploye == null || (e.Grade == modelEmploye.Grade && e.EmployeCode != modelEmploye.EmployeCode);
                    break;
                default:
                    filterData = e => true;
                    break;
            }

            var returnData = catalog.Select(async l => new CalculationsEmployesSalaryViewDtos
            {
                BeginDate = l.BeginDate,
                Grade = l.Grade,
                BirthDay = l.BirthDay,
                Divicion = l.Divicion,
                Office = l.Office,
                EmployeCode = l.EmployeCode,
                EmployeName = l.EmployeName,
                EmployeSurname = l.EmployeSurname,
                IdentificationNumber = l.IdentificationNumber,
                Position = l.Position,
                TotalSalary = await _calculateEmployesSalaryService.CalculateTotalSalary(l.BaseSalary, l.ProductionBonus, l.CompensationBonus, (await _calculateEmployesSalaryService.CalculateOtherIncome(l.BaseSalary, l.Commission)), l.Contributions)
            }).Select(l => l.Result).Where(filterData).ToList();



            if (returnData.Any())
            {
                return PartialView(returnData);
            }
            return PartialView(new List<CalculationsEmployesSalaryViewDtos>());


        }

        public ActionResult LastSalaries()
        {
            return View(new LastSalariesDto
            {
                Birthday = new DateTime(1900, 01, 01),
                EmployeName = "N/A",
                EmployeCode = "N/A",
                EmployeSurname = "N/A",
                BeginDate = new DateTime(1900, 01, 01),
                Division = "N/A",
                Grade = 0,
                IdentificationNumber = "N/A",
                Position = "N/A",
                Office = "N/A",
                LastSalariesDetails = new List<LastSalariesDetailsDto>()

            });
        }
        [HttpPost]
        public async Task<ViewResult> LastSalaries(LastSalariesDto lastSalariesDto)
        {
            var resultData = (await _employeMasterSalaryService.GetEmployeMasterSalaries()).Where(l => l.EmployeCode == lastSalariesDto.EmployeCode).ToList()
                .OrderBy(l => l.Year + l.Month).Take(3).ToList();

            var lastSalariesData = _mapper.Map<List<LastSalariesDto>>(resultData).Distinct().FirstOrDefault();

            if (lastSalariesData != null)
            {
                lastSalariesData.LastSalariesDetails = (resultData.Any()
                    ? _mapper.Map<List<LastSalariesDetailsDto>>(resultData).Select(async l => new LastSalariesDetailsDto
                    {
                        Year = l.Year,
                        Month = l.Month,
                        BaseSalary = l.BaseSalary,
                        Bonus = Math.Round(
                            (await _calculateEmployesSalaryService.CalculateBonusLastSalaries(l.BaseSalary)), 2)
                    }).Select(l => l.Result).ToList()
                    : new List<LastSalariesDetailsDto>());

                return View(lastSalariesData);
            }

            return null;
        }
    }
}
